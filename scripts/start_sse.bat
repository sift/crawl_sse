@echo off
REM ***************************************************************************
REM @Contact :   liuyuqi.gov@msn.cn
REM @Time    :   2024/07/03 18:54:02
REM @Version :   1.0
REM @License :   (C)Copyright 2019 liuyuqi.
REM @Desc    :   None
REM %1 - ext_name
REM %2 - characters replaced
REM %3 - new characters
REM ***************************************************************************
crawl_sse nianbao --download
REM ./crawl_sse nianbao --download
