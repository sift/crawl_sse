#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Contact :   liuyuqi.gov@msn.cn
@Time    :   2024/06/22
@License :   Copyright © 2017-2022 liuyuqi. All Rights Reserved.
@Desc    :   command line params or config from file
"""

import sys,os,re
import argparse
from collections import OrderedDict
from .utils.frozen_dir import get_app_path

def parse_args():
    """
    parse command line params
    """
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description='''
            crawl_sse
            command line params:
            python main.py crawl --extractor cninfo
            python main.py download --extractor cninfo
            python main.py crawl --extractor sse
                                     ''', epilog="Believe that with the above description, " +
                                                            "you can start working right away. Wish you success")
    # group = parser.add_mutually_exclusive_group()
    parser.add_argument('command',  help='command: crawl, download', choices=['crawl','download', 'help','version'] , default='help')
    parser.add_argument('--extractor', help='extractor: cninfo, sse', choices=['cninfo','sse'], default='cninfo')
    args = parser.parse_args()

    if args is None:
        parser.print_help()
        sys.exit(1)
    # remove None
    command_line_conf = OrderedDict(
        {k: v for k, v in args.__dict__.items() if v is not None}
    )
    system_conf = user_conf = custom_conf = OrderedDict()
    system_conf.update(command_line_conf)

    app_path = get_app_path()
    system_conf["app_path"] = app_path
    return system_conf

def _read_custom_conf(config_path: str) -> OrderedDict:
    """read custom config file"""
    pass

def _read_user_conf() -> OrderedDict:
    """read user config file"""
    pass
