#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@Contact :   liuyuqi.gov@msn.cn
@Time    :   2024/07/05
@License :   Copyright © 2017-2022 liuyuqi. All Rights Reserved.
@Desc    :   insntall brawser with selenium
"""

from selenium import webdriver
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.edge.service import Service

edge = webdriver.Edge(service=Service(EdgeChromiumDriverManager().install()))
# edge = webdriver.EdgeService(EdgeChromiumDriverManager().install())
# chrome = webdriver.ChromeService(ChromeDriverManager().install())
