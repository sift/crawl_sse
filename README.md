# crawl_sse


[![Version](https://img.shields.io/badge/version-v1.1.0-brightgreen)](https://git.yoqi.me/lyq/crawl_sse)
[![Python](https://img.shields.io/badge/Python-3.11.4-brightgreen?style=plastic)](https://git.yoqi.me/lyq/crawl_sse)

[English](./README.md) 

上市公司数据下载，分析

## Develop

抽取上市公司数据：
```
poetry shell
python main.py crawl --extractor sse

python main.py crawl --extractor cninfo
#上市公司年报下载：
python main.py download --extractor cninfo

```

docker 打包交付运行：

```
# docker run -d -p 9515:9515 -v $(pwd):/app mcr.microsoft.com/msedge/msedgedriver

docker run -it --rm -v /data/crawl_sse:/app jianboy/crawl_sse:1.0.1 download --extractor cninfo

```


分析年报：
docs/上市公司分析.ipynb


## License

Licensed under the [Apache 2.0](LICENSE) © [liuyuqi.gov@msn.cn](https://github.com/jianboy)



## Reference
