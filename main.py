#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@Contact :   liuyuqi.gov@msn.cn
@Time    :   2023/12/03 03:09:35
@License :   Copyright © 2017-2022 liuyuqi. All Rights Reserved.
@Desc    :   enter point
多个功能，整理成命令行参数执行
'''

from crawl_sse import Sse
from crawl_sse import Cninfo
from crawl_sse.options import parse_args
import sys

if __name__=='__main__':
    args = parse_args()
    if args['command'] == 'crawl':
        if args['extractor'] == 'sse':
            sse = Sse()
            sse.crawl()
        elif args['extractor'] == 'cninfo':
            cninfo = Cninfo()
            cninfo.crawl()
    elif args['command'] == 'download':
        if args['extractor'] == 'cninfo':
            cninfo =Cninfo()
            cninfo.download()
    elif args['command'] == 'help':
        pass
    elif args['command'] == 'version':
        print('1.0.0')
    else:
        print('command error, please use --help to get help')
        sys.exit(1)
